/**
 * File : addUser.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addUserForm = $("#addUser");
	
	var validator = addUserForm.validate({
		
		// rules:{
		// 	fname :{ required : true },
		// 	email : { required : true, email : true, remote : { url : baseURL + "checkEmailExists", type :"post"} },
		// 	password : { required : true },
		// 	cpassword : {required : true, equalTo: "#password"},
		// 	mobile : { required : true, digits : true },
		// 	role : { required : true, selected : true}
		// },
		// messages:{
		// 	fname :{ required : "This field is required" },
		// 	email : { required : "This field is required", email : "Please enter valid email address", remote : "Email already taken" },
		// 	password : { required : "This field is required" },
		// 	cpassword : {required : "This field is required", equalTo: "Please enter same password" },
		// 	mobile : { required : "This field is required", digits : "Please enter numbers only" },
		// 	role : { required : "This field is required", selected : "Please select atleast one option" }
		// }
	});
});
function getFormData() {

	var data = {};

	data.cid = $('#cid').val();
	data.sid= $('#sname').val();


	data = JSON.stringify(data);
	return data;
}

function submitStudent() {

		var baseurl = document.getElementById('baseurl').value;
		var data = getFormData();

		$.ajax({
			type: "POST",
			url: baseurl + "View_courses/set_assign",
			data: {data: data},
			success: function (data) {
				console.log(data);
				if (data==true) {

						alertify.success('Course Assigned!');
					$("#action").val('insert');
				} else {
					alertify.error('Course Assign Failed!');

				}
			}
		});
	// }


}