/**
 * File : addUser.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addUserForm = $("#addUser");
	
	var validator = addUserForm.validate({
		
		// rules:{
		// 	fname :{ required : true },
		// 	email : { required : true, email : true, remote : { url : baseURL + "checkEmailExists", type :"post"} },
		// 	password : { required : true },
		// 	cpassword : {required : true, equalTo: "#password"},
		// 	mobile : { required : true, digits : true },
		// 	role : { required : true, selected : true}
		// },
		// messages:{
		// 	fname :{ required : "This field is required" },
		// 	email : { required : "This field is required", email : "Please enter valid email address", remote : "Email already taken" },
		// 	password : { required : "This field is required" },
		// 	cpassword : {required : "This field is required", equalTo: "Please enter same password" },
		// 	mobile : { required : "This field is required", digits : "Please enter numbers only" },
		// 	role : { required : "This field is required", selected : "Please select atleast one option" }
		// }
	});
});
function getFormData() {
	var studentid = $('#StudentId').val();

	var student = {};
	if ($("#status").is(":checked")){
		student.status=1;
		console.log('ac');

	} else {
		student.status=0;
		console.log('dc');
	}
	student.id = $('#StudentId').val();
	student.name = $('#StudentName').val();
	student.nic = $('#StudentNIC').val();
	student.address = $('#address1').val();
	student.address1 = $('#address2').val();
	student.address2 = $('#address3').val();
	student.faculty = $('#faculty').val();
	student.batch = $('#batch').val();
	student.tel = $('#telephone').val();
	student.email = $('#email').val();
	var data = {};
	data.id =studentid;
	data.student = student;

	data = JSON.stringify(data);
	return data;
}

function submitStudent() {

		var baseurl = document.getElementById('baseurl').value;
		console.log(baseurl);
		var data = getFormData();

		$.ajax({
			type: "POST",
			url: baseurl + "Student/set_student",
			data: {data: data},
			success: function (data) {
				console.log(data);
				if (data.student) {
					// if (data.action === 'insert') {
						alertify.success('New Student ' + data.data.id + ' Inserted!');
					// } else if (data.action === 'update') {
					// 	alertify.success('Customer ' +  data.student.id  + ' Updated');
					// }

					$("#action").val('insert');
				} else {
					// if (data.action === 'insert') {
						alertify.error('Insert Failed!');
					// } else if (data.action === 'update') {
					// 	alertify.error('No Changes to Update');
					// }
				}
			}
		});
	// }


	function getStudents() {
		if ($('#status').val() === 'edit') {
			var baseurl = $('#baseurl').val();
			var userId = $('#editUserId').val();

			$.ajax({
				type: "POST",
				url: baseurl + "newstudent/get_student",
				data: "id=" + userId,
				success: function (data) {
					console.log(data);
					setStudents(data);
				}
			});
		}
	}
	function setStudents(user) {
//    var userId = user.userId;
		$('#username').prop('readonly', true);
		var userName = user.UserName;
		var email = user.email;
		var name = user.name;
		var role = user.role;
		var password = user.Password;
		$('#fullname').val(name);
		$('#email').val(email);
		$('#role').val(role);
		$('#username').val(userName);
		$('#password').val(password);
		$('#repassword').val(password);

	}
}