/**
 * File : addUser.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addUserForm = $("#addUser");
	
	var validator = addUserForm.validate({
		
		// rules:{
		// 	fname :{ required : true },
		// 	email : { required : true, email : true, remote : { url : baseURL + "checkEmailExists", type :"post"} },
		// 	password : { required : true },
		// 	cpassword : {required : true, equalTo: "#password"},
		// 	mobile : { required : true, digits : true },
		// 	role : { required : true, selected : true}
		// },
		// messages:{
		// 	fname :{ required : "This field is required" },
		// 	email : { required : "This field is required", email : "Please enter valid email address", remote : "Email already taken" },
		// 	password : { required : "This field is required" },
		// 	cpassword : {required : "This field is required", equalTo: "Please enter same password" },
		// 	mobile : { required : "This field is required", digits : "Please enter numbers only" },
		// 	role : { required : "This field is required", selected : "Please select atleast one option" }
		// }
	});
});
function getFormData() {
	var courseid = $('#cid').val();

	var course = {};
	if ($("#status").is(":checked")){
		course.status=1;
	} else {
		course.status=0;
;
	}
	course.cid = $('#cid').val();
	course.cname = $('#cname').val();
	course.cfaculty = $('#cfaculty').val();
	course.enrolkey = $('#enrolkey').val();

	var data = {};
	data.id =courseid;
	data.course = course;

	data = JSON.stringify(data);
	return data;
}

function submitStudent() {

		var baseurl = document.getElementById('baseurl').value;
		console.log(baseurl);
		var data = getFormData();

		$.ajax({
			type: "POST",
			url: baseurl + "Course/set_course",
			data: {data: data},
			success: function (data) {
				console.log(data);
				if (data==true) {
					// if (data.action === 'insert') {
						alertify.success('New Course Inserted!');
					// } else if (data.action === 'update') {
					// 	alertify.success('Customer ' +  data.course.id  + ' Updated');
					// }
					// resetCustomer();
					$("#action").val('insert');
				} else {
					// if (data.action === 'insert') {
						alertify.error('Insert Failed!');
					// } else if (data.action === 'update') {
					// 	alertify.error('No Changes to Update');
					// }
				}
			}
		});
	// }


}