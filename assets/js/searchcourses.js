$(function () {

    getStudents();
});

function getStudents() {
    var baseurl = document.getElementById('baseurl').value;
//    $("#tblItemSearch > tbody").html("");
    $.ajax({
        type: 'POST',
        url: baseurl + 'View_courses/get_Courses',
        success: function (data) {
            setStudents(data);
            $('#tblCourses').DataTable();

        }

    });
}
function setStudents(students) {
    // if ($.fn.DataTable.isDataTable('#tblCourses')) {
    //     $('#tblCourses').DataTable().destroy();
    // }
    $('#tblCourses tbody').empty();
    var textToInsert = '';
    for (var item in students) {
        textToInsert += addRowToSearchStudent(students[item]);
    }
    $('#tblCourses > tbody:last-child').append(textToInsert);
    $('#tblCourses').dataTable({

    });
}
function addRowToSearchStudent(item) {
    var baseurl = document.getElementById('baseurl').value;
//    console.log(item.ItemCode);
    var row =
        '<tr id="' + item.cid + '" class="openPane">'
        + '<td>' + item.cid + '</td>'
        + '<td>' + item.cname + '</td>'
        + '<td>' + item.cfaculty + '</td>'
        + '<td>' + item.enrolkey + '</td>'
        + '<td>'
        + '<a href="' + baseurl + 'view_courses/addCourse/' + item.cid + '"><button type="button" class="btn btn-info btn-xs">Add</button></a>'
        + '</td>'
        + '</tr>';
    return row;
}